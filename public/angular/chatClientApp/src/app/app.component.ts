import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', "main.sass", "./styles/bootstrap.min.css"]
})
export class AppComponent {
  title = 'app';
}
