
export class Message {
  id: number;
  from: string;
  text: string;
  idRoom: number;
  date: string;
  attachment: {
    id: number;
    path: string;
  }
}
