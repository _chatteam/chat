import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent, RegistrationComponent } from './user/index';
import { ChatComponent } from './user/components/chat.component';
import { ChatBoardComponent } from './user/components/chatBoard.component';


const routes: Routes = [
    { path: '', component: LoginComponent },
    { path: 'registration', component: RegistrationComponent },
    {   path: 'chat', component: ChatComponent,
        children: [
            { path: ':chat', component: ChatBoardComponent }
        ]
    }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})


export class AppRoutingModule { }
