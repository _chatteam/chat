import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { ServerService } from './user/webServices/index';
import { AppRoutingModule } from './/app-routing.module';
import { LoginComponent, RegistrationComponent } from './user/index';
import { ChatComponent } from './user/components/chat.component';
import { ChatBoardComponent } from './user/components/chatBoard.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    ChatComponent,
    ChatBoardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [ ServerService ],
  bootstrap: [AppComponent, LoginComponent, RegistrationComponent, ChatComponent, ChatBoardComponent],
  entryComponents: [ ChatComponent ]
})
export class AppModule { }
