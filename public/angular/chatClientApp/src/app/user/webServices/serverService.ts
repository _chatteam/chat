import {Injectable} from "@angular/core";
import * as io from 'socket.io-client';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class ServerService {
  socket = io("http://localhost:3000/");
  token = "";

  chatList: {}[];
  friendList: {}[];

  socketRooms$ = new BehaviorSubject<any>([]);

  constructor() {
    this.token = ServerService.getCookie("token");
        this.socketRooms$.asObservable().subscribe( result => {
            if (result.success) {
                this.chatList = result.rooms;
            }
        });
    }

    initSettings = (token: string): void => {
        this.token = token;
    };

    loadRoom = (): void => {
        this.socket.emit("loadRoom", { token: this.token }, (result) => this.socketRooms$.next(result));
    };

  sessionInfo = (): void => {
    this.socket.emit('sessionInfo', {token: this.token}, (result)=>{
      this.friendList = result;
    })
  };


   static getCookie (name) {
    let matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

}
