import {Component, OnInit} from '@angular/core';
import {ServerService} from '../webServices/index';
import {User} from '../../shared/models/user';
import {Router} from '@angular/router';

@Component({
  selector: 'login-component',
  templateUrl: './login.component.html',
  styleUrls: []
})
export class LoginComponent implements OnInit {
  loading = false;
  user: User = new User();
  model: any = {};
  token;
  info;

  constructor(private router: Router,
              private serverService: ServerService) {
    this.token = this.serverService.token;
    let data ={
    token: this.token
    };
    this.serverService.socket.emit("getUserInfo", data, (result) => {
      if (result.success) {
        this.router.navigate(['/chat']);
      }
    })
  }

  ngOnInit() {

  }

  loginUser = (): void => {
    this.loading = true;
    this.serverService.socket.emit("login", this.user, (result) => this.successedLogin(result)
    );
  };

  successedLogin = (result: any): void => {
    if (result.success) {
      document.cookie = "token=" + result.data.token;
      this.serverService.initSettings(result.data.token);
      this.router.navigate(['/chat']);
    }
  }
}
