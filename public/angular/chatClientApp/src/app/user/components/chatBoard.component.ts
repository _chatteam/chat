import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-channel',
  templateUrl: './chatBoard.component.html',
  styleUrls: ["../../styles/chatBoard.css"],
  moduleId: module.id,
})
export class ChatBoardComponent implements OnInit {

    currentMsg = '';

    constructor(
        private activatedRoute: ActivatedRoute
    ) { }

    send() {
        if (this.currentMsg.length) {
            this.currentMsg = "";
        }
    }

    ngOnInit() {
        
            this.activatedRoute.params.subscribe(params => {
              console.log(params);        
            })
          }

}