import {Component, OnInit, Input} from '@angular/core';
import {Router} from "@angular/router";
import {ServerService} from '../webServices/index';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  //styleUrls: ["../../main.sass"],
  styleUrls: ["../../styles/chats.css"],
  moduleId: module.id,
})
export class ChatComponent {

  constructor(private router: Router,
              private serverService: ServerService) {
    this.loadRoom();
    this.sessionInfo();
  }

    update = () => {
        console.log("test");
    }

    leaveChat(frq) {
    }

  public loadRoom = (): void => {
    this.serverService.loadRoom();
  };
  public sessionInfo = (): void => {
    this.serverService.sessionInfo();
  };
}
