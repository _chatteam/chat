import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {ServerService} from '../webServices/index';
import {User} from '../../shared/models/user';

@Component({
  selector: 'registration-component',
  templateUrl: './registration.component.html'
})

export class RegistrationComponent {
  user: User = new User();
  model: any = {};
  loading = false;

  constructor(private router: Router,
              private serverService: ServerService) {

  }

    register(): void {
        this.loading = true;
        this.serverService.socket.emit('register', this.user, (result) => this.successedRegister(result));
    }

    successedRegister = (result: any): void => {
        if (result.success) {
            document.cookie = "token=" + result.data.token;
            this.router.navigate(["/chat"]);
        }
    }
}
