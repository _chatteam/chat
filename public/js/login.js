function login() {
    var login = $("input[name='username']").val();
    var password = $("input[name='password']").val();
    _login(login, password, function (data) {
        if (data.success) {
            document.cookie = `username = ${data.data.token}`;
            location.reload();
        }
        else console.log("responce", data);
    })
}

function _login(login, password, callback) {
    socket.emit('login', {login, password},
        function (data) {
            callback(data);
        });
}