function reg() {
    const firstName = $("input[name='firstname']").val(),
        lastName = $("input[name='lastname']").val(),
        login = $("input[name='login']").val(),
        pass = $("input[name='password']").val();

    _reg(firstName, lastName, login, pass, function (data) {
        if (data.success) {
           //В ответ НЕ ПРИХОДИТ ТОКЕН, нужно авторизоваться
        }
    });
}

function _reg(firstName, lastName, login, pass, callback) {
    socket.emit('register', {login: login, password: pass, lastName: lastName, firstName: firstName},
        function (data) {
            callback(data);
        });
}

