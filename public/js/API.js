/**
 *
 * Все запросы по сокету обернуты в функции для тестирования,
 * удобного вызова и/или легкой интеграции.
 *
 */
let socket;

(function connection(_url = "http://localhost:3000") {
    socket = io.connect(_url);
})();

function connect() {
    return socket.connected;
}

function registration(firstName, lastName, login, pass, callback) {
    socket.emit('register', {login: login, password: pass, lastName: lastName, firstName: firstName},
        function (data) {
            callback(data);
        });
}

function deleteUser(login, password, callback) {
    socket.emit('deleteUser', {login, password},
        function (data) {
            callback(data);
        });
}

function authentication(login, password, callback) {
    socket.emit('login', {login, password},
        function (data) {
            callback(data);
        });
}


/**
 *
 * @param users - массив пользователей, добавляемых в диалог
 * @param name - название диалога
 * @param token - токен клиента
 * @param callback - информация о созданной комнате. Структура ответа  {id, date}
 *
 */
function createRoom(token, name, users, callback) {
    socket.emit('createRoom', {token, name, users}, function (data) {
        callback(data);
    });
}

function addInRoom(token, idRoom, users, callback) {
    socket.emit('addInRoom', {token, idRoom, users}, function (data) {
        callback(data);
    });
}

function deleteFromRoom(token, idRoom, callback) {
    socket.emit('deleteFromRoom', {token, idRoom}, function (data) {
        callback(data);
    });
}

function deleteUsersFromRoom(token, login, idRoom, callback) {
    socket.emit('deleteUsersFromRoom', {token, login, idRoom}, function (data) {
        callback(data);
    });
}

function deleteRoom(token, idRoom, callback) {
    socket.emit('deleteRoom', {token, idRoom}, function (data) {
        callback(data);
    });
}


function addMessage(token, idRoom, text, callback) {
    socket.emit('addMessage', {idRoom, text, token}, function (data) {
        callback(data);
    });
}

function deleteMessage(token, idRoom, idMessage, callback) {
     socket.emit('deleteMessage', {token, idRoom, idMessage}, function (res) {
        callback(res);
    });
}

/**
 *
 * @param idRoom - id комнаты
 * @param limit - лимит сообщений, которых нужно подгрузить
 * @param callback - массив сообщений rows[] структура rows[i]{id - id сообщения, sendFrom - отпарвитель,
  * date - дата/время, text - текстовое сообщение, file_path - путь к файлу(для загрузки с сервера), img_path - путь к изображению на сервере}
 */
function showMessage(token, idRoom, limit, callback) {
    socket.emit('showMessage', {token, idRoom, limit}, function (result) {
        callback(result);
    });
}

function showRoom(token, callback) {
    socket.emit('showRoom', {token}, function (res) {
        callback(res);
    });
}


function addFriend(token,friend, callback) {
    socket.emit('addFriend', {token,friend}, function (res) {
        callback(res);
    });
}
function showFriend(token, callback) {
    socket.emit('showFriendList', {token}, function (res) {
        callback(res);
    });
}


function deleteFriend(token,friend, callback) {
    socket.emit('deleteFriend', {token,friend}, function (res) {
        callback(res);
    });
}

function getUserInfo(token,callback) {
    socket.emit('getUserInfo', {token}, function (res) {
        callback(res);
    });
}

socket.on("online", (data)=>{
    console.log("online ",data);
});
socket.on("addInRoom", (data)=>{
    console.log("addInRoom ",data);
});
socket.on("addInFriend", (data)=>{
    console.log("addInFriend ",data);
});