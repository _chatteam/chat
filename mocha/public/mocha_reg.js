describe("Регистрация", function () {

    const rand = new Date().getMilliseconds(),
        USER = "testRegistr" + rand,
        USER_SYMB = "[~#&];,:" + rand;
    let userToken, symbolToken;
    //чистим тестовые логины
    after((done) => {
        deleteUser(USER, USER, function (callback) {
            done();
        });
    });


    it("нового пользователя", function (done) {
        let fn = ln = lg = ps = USER;
        registration(fn, ln, lg, ps, function (callback) {
            try {
                assert.equal(callback.success, true);
                userToken = callback.data.token;
                done();
            } catch (e) {
                done(e)
            }
        });
    });

    it("существующего пользователя", function (done) {
        let fn = ln = lg = ps = USER;
        registration(fn, ln, lg, ps, function (callback) {
            try {
                assert.equal(callback.success, false);
                done();
            } catch (e) {
                done(e)
            }
        });
    });

    it("пустого пользователя", function (done) {
        let fn = ln = lg = ps = '';
        registration(fn, ln, lg, ps, function (callback) {
            try {
                assert.equal(callback.success, false);
                done();
            } catch (e) {
                done(e)
            }

        });
    });
    it("undefined", function (done) {
        let fn = ln = lg = ps;
        registration(fn, ln, lg, ps, function (callback) {
            try {
                assert.equal(callback.success, false);
                done();
            } catch (e) {
                done(e)
            }

        });
    });
    it("длинного пользователя", function (done) {
        let fn = ln = lg = ps = '                                                                               ';
        registration(fn, ln, lg, ps, function (callback) {
            assert.equal(callback.success, false);
            done();
        });
    });

    it("спецсимвольного пользователя", function (done) {
        let fn = ln = lg = ps = USER_SYMB;
        registration(fn, ln, lg, ps, function (callback) {
            try {
                assert.equal(callback.success, true, callback.info);
                symbolToken = callback.data.token;
                symbolToken = callback.data.token;
                done();
            } catch (e) {
                done(e)
            }

        });
    });

    it("Удаление пользователя", function (done) {
        deleteUser(USER_SYMB, USER_SYMB, function (callback) {
            done();
        });
    });

});