describe("Сообщения", () => {

    let text = "test text",
        url = "https://www.google.ru/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png",
        rand = new Date().getMilliseconds(),
        name = "testMessages" + rand,
        USER = "TeStMeSsAgE" + rand,
        USER_SYMB = "[~#&];,:3" + rand,
        room,
        id,
        token;


    before((done) => {
        registration(USER, USER, USER, USER, function (callback) {
            registration(USER_SYMB, USER_SYMB, USER_SYMB, USER_SYMB, function (callback) {
                authentication(USER, USER, function (callback) {
                    token = callback.data.token;
                    createRoom(token, name, [USER_SYMB], function (result) {
                        room = result.data.idRoom;
                        done();
                    });
                });
            });
        });
    });

    after((done) => {
        deleteUser(USER, USER, function (callback) {
            deleteUser(USER_SYMB, USER_SYMB, function (callback) {
                done();
            });
        });
    });


    describe("Отправка сообщений", function () {

        describe("Отправка текста", function () {

            it("Сообщение отправлено", function (done) {
                addMessage(token, room, text, function (data) {
                    assert.equal(data.success, true);
                    done();
                });
            });

            it("Успешно вернуло сообщение", function (done) {
                addMessage(token, room, text, function (data) {
                    assert.equal(data.data.message, text);
                    id = data.data.id;
                    done();
                });
            });

            it("Удаление сообщения", function (done) {
                deleteMessage(token, room, id, function (data) {
                    assert.equal(data.success, true);
                    done();
                });
            });
        });

        describe("Отправка картинки", function () {

            it("Картинка отправлена", function (done) {
                addMessage(token, room, url, function (data) {
                    assert.equal(data.success, true);
                    done();
                });
            });

            it("Формат подтверждён", function (done) {
                addMessage(token, room, url, function (data) {
                    assert.notEqual(data.data.attachment, undefined);
                    done();
                });
            });

            it("Картинка скачена", function (done) {
                addMessage(token, room, url, function (data) {
                    assert.notEqual(data.data.attachment.imgPath, undefined);
                    done();
                });
            });
        });

        describe("Обработка исключений", function () {

            it("Пустое сообщение", function (done) {
                addMessage(token, room, "", function (data) {
                    assert.equal(data.success, false);
                    done();
                });
            });

            it("Сообщение из пробелов", function (done) {
                addMessage(token, room, "               ", function (data) {
                    assert.equal(data.success, false, "Сообщение из пробелов. info => " + data.info);
                    done();
                });
            });

            it("Сообщение без указания диалога", function (done) {
                addMessage(token, "", text, function (data) {
                    assert.equal(data.success, false);
                    done();
                });
            });

            it("Отправитель не подписан", function (done) {
                addMessage("", room, text, function (data) {
                    assert.equal(data.success, false);
                    done();
                });
            });

            it("Экранирование спецсимволов", function (done) {
                addMessage(token, room, "<Script>...</Script>", function (data) {
                    assert.equal(data.success, true);
                    done();
                });
            });

            url = "before https://www.google.ru/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png after";

            it("Распознование ссылки среди текста и загрузка изображения", function (done) {
                addMessage(token, room, url, function (data) {
                    assert.notEqual(data.data.attachment.imgPath, undefined);
                    done();
                })
            });

            it("Остаток сообщения без ссылки", function (done) {
                addMessage(token, room, url, function (data) {
                    assert.notEqual(data.data.message, undefined);
                    done();
                })
            });
        });
    });

    describe("Загрузка сообщений", function () {
        let limit = 20;

        it("из тестовой комнаты", function (done) {
            showMessage(token, room, limit, function (data) {
                assert.equal(data.success, true);
                done();
            })
        });

        it("из ваакума", function (done) {
            showMessage(token, '', limit, function (data) {
                assert.equal(data.success, false);
                done();
            })
        });

        it("без указания лимита выгрузки", function (done) {
            showMessage(token, room, '', function (data) {
                assert.equal(data.success, false);
                done();
            })
        });
    });

});


describe("Friend list", () => {

    let rand = new Date().getMilliseconds(),
        USER = "TeStMeSsAgE" + rand,
        USER_SYMB = "[~#&];,:3" + rand,
        token;


    before((done) => {
        registration(USER, USER, USER, USER, function (callback) {
            registration(USER_SYMB, USER_SYMB, USER_SYMB, USER_SYMB, function (callback) {
                authentication(USER, USER, function (callback) {
                    token = callback.data.token;
                    done();
                });
            });
        });
    });

    after((done) => {
        deleteUser(USER, USER, function (callback) {
            deleteUser(USER_SYMB, USER_SYMB, function (callback) {
                done();
            });
        });
    });


    describe("Добавление в список", function () {

        it("Успешное добавление", function (done) {
            addFriend(token, USER_SYMB, function (data) {
                assert.equal(data.success, true);
                done();
            });
        });
        it("Выгрузка списка друзей", function (done) {
            showFriend(token, function (data) {
                assert.equal(data.success, true);
                done();
            });
        });
        it("Ошибка при добавление самого себя", function (done) {
            addFriend(token, USER, function (data) {
                assert.equal(data.success, false);
                done();
            });
        });

        it("Удаление из списка друзей", function (done) {
            deleteFriend(token, USER_SYMB, function (data) {
                assert.equal(data.success, true);
                done();
            });
        });


    });

});