describe("Авторизация", function () {

    const rand = new Date().getMilliseconds(),
        USER = "testLogin1" + rand,
        USER_SYMB = "[~#&];,:1" + rand;
    let userToken, symbolToken;
    before((done) => {
        registration(USER, USER, USER, USER, function (callback) {
            registration(USER_SYMB, USER_SYMB, USER_SYMB, USER_SYMB, function (callback) {
                done();
            });
        });
    });

    after((done) => {
        deleteUser(USER,USER, function (callback) {
            deleteUser(USER_SYMB,USER_SYMB, function (callback) {
                done();
            });
        });
    });

    it("созданного пользователя", function (done) {
        authentication(USER, USER, function (callback) {
            try {
                assert.equal(callback.success, true);
                userToken = callback.data.token;
                done();
            } catch (e) {
                console.log(e);
                done(e)
            }
        });
    });


    it("символьного пользователя", function (done) {
        authentication(USER_SYMB, USER_SYMB, function (callback) {
            try {
                assert.equal(callback.success, true);
                done();
            } catch (e) {
                done(e)
            }

        });
    });

    it("Получение информации по токену", function (done) {
        getUserInfo(userToken, function (callback) {
            try {
                assert.equal(callback.success, true);
                done();
            } catch (e) {
                done(e)
            }

        });
    });


    it("не выполнена. Неверный логин/пароль", function (done) {
        authentication(USER, USER_SYMB, function (callback) {
            try {
                assert.equal(callback.success, false);
                done();
            } catch (e) {
                done(e)
            }

        });
    });

    it("не выполнена. Пустой логин", function (done) {
        authentication("", USER, function (callback) {
            try {
                assert.equal(callback.success, false);
                done();
            } catch (e) {
                done(e)
            }

        });
    });

    it("не выполнена. Пустой пароль", function (done) {
        authentication(USER, "", function (callback) {
            try {
                assert.equal(callback.success, false);
                done();
            } catch (e) {
                done(e)
            }
        });
    });

    it("не выполнена. Слишком длинный логин", function (done) {
        authentication(USER + USER + USER + USER, USER, function (callback) {
            try {
                assert.equal(callback.success, false);
                done();
            } catch (e) {
                done(e)
            }

        });
    });

    it("не выполнена. Слишком длинный пароль", function (done) {
        authentication(USER, USER + USER + USER + USER, function (callback) {
            try {
                assert.equal(callback.success, false);
                done();
            } catch (e) {
                done(e)
            }

        });
    });

});