describe("Создание диалога", function () {

    let rand = new Date().getMilliseconds(),
        USER = "testDialUS" + rand,
        USER_SYMB = "[~#&];,:2" + rand,
        name1 = "testDial1R",
        name2 = 'testDial2R',
        room1,
        room2,
        token,
        token2;

    //Регистрация пользователя и подписка на сервере
    before((done) => {
        registration(USER, USER, USER, USER, function (callback) {
            registration(USER_SYMB, USER_SYMB, USER_SYMB, USER_SYMB, function (callback) {
                authentication(USER, USER, function (callback) {
                    token = callback.data.token;
                    authentication(USER_SYMB, USER_SYMB, function (callback) {
                        token2 = callback.data.token;
                        done();
                    });
                });
            });
        });
    });

    //Подчищаем тестовую инфу
    after((done) => {
        deleteUser(USER, USER, function (callback) {
            deleteUser(USER_SYMB, USER_SYMB, function (callback) {
                done();
            });
        });
    });

    it("Тет-а-тет", function (done) {
        createRoom(token, name1, [USER_SYMB], function (result) {
            room1 = result.data.idRoom;
            assert.equal(result.success, true);
            done();
        });
    });

    it("Без собеседника", function (done) {
        let array = [];
        createRoom(token, name2, array, function (result) {
            room2 = result.data.idRoom;
            assert.equal(result.success, true);
            done();
        });
    });

    it("Добавление в существующую комнату(чат)", function (done) {
        addInRoom(token, room2, [USER_SYMB], function (result) {
            assert.equal(result.success, true);
            done();
        });
    });


    it("Выгрузка комнат(чатов)", function (done) {
        showRoom(token, function (result) {
            assert.equal(result.success, true);
            done();
        });
    });

    it("Выход из чата", function (done) {
        deleteFromRoom(token2, room1, function (result) {
            assert.equal(result.success, true);
            done();
        });
    });
    it("Удаление из чата", function (done) {
        deleteUsersFromRoom(token, USER_SYMB, room2, function (result) {
            assert.equal(result.success, true);
            done();
        });
    });
    it("Удаление чата", function (done) {
        deleteRoom(token, room2, function (result) {
            assert.equal(result.success, true);
            done();
        });
    });

    it("С пустым названием", function (done) {
        createRoom(token, "", USER_SYMB, function (result) {
            assert.equal(result.success, false);
            done();
        });
    });

    it("Без токена", function (done) {
        createRoom("", name, USER_SYMB, function (result) {
            assert.equal(result.success, false);
            done();
        });
    });

    it("Ошибка. undefined", function (done) {
        createRoom(undefined, undefined, undefined, function (result) {
            assert.equal(result.success, false);
            done();
        });
    });
});